# AppAtBox project template v1.0.1
> Стартовый шаблон для быстрого начала работы над проектом
 
## Что нового
 - Добавлена сетка от bootstrap 3.5
 - Созданы css файлы для прозрачной mobile-first верстки
 - Добавлена сорторовка сборки css и js

## Начало работы

```shell
git clone git@gitlab.com:irbisadm/appatbox-template.git
cd appatbox-template
git remote remove origin
npm install --save-dev
```
## Запуск watch
Для работы над проектом запуститепше 
```shell
npm grunt
```
